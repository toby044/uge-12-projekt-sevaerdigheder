<html>
<?php
include('../modules/head.php');
include('../modules/header.php');
include('../rest/test_api.php');

?>
<form action="" method="post">
    <input type="search" name="searchInput" id="searchInput" placeholder="Indtast adresse">

    <button type="submit">Søg</button>
</form>

<?php
if ($_POST) {

    // var_dump($_POST);

    $arr = getAdresse($_POST['searchInput']);

    $adresse_korrekt = $arr->adgangsadresse->adressebetegnelse;
    $kommune_kode = $arr->adgangsadresse->kommune->kode;
    $koordinater_lng = $arr->adgangsadresse->adgangspunkt->koordinater[0];
    $koordinater_lat = $arr->adgangsadresse->adgangspunkt->koordinater[1];


    // var_dump($adresse_korrekt);
    // var_dump($kommune_kode);
    // var_dump($koordinater_lat);
    // var_dump($koordinater_lng);

    echo '<p>';
    echo "Du søgte på <br> $adresse_korrekt. <br> Den ligger i kommunekode $kommune_kode, og har koordinaterne LAT: $koordinater_lat, LONG: $koordinater_lng.";
    echo '</p>';
    // var_dump($arr);

}

?>

<div id="map" style="height:30vh; width: 50vw"></div>

<script>
    var map = L.map('map').setView([<?= $koordinater_lat ?>, <?= $koordinater_lng ?>], 15);
    var marker = L.marker([<?= $koordinater_lat ?>, <?= $koordinater_lng ?>]).addTo(map);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibW9ydGVucnVkIiwiYSI6ImNsMTVrOWsxZzB0c3AzbHJ0aDJ5ZHdxaWUifQ.d0ehjvZ-RQyxTn_VQTZtvw'

    }).addTo(map);
</script>


</html>