<html>
<?php
include('../modules/head.php');
include('../modules/header.php');
?>

<body class="index-bg">
    <main class="index-main">
        <div class="index-main-welcome-wrapper">
            <h1>Ancient monuments in Denmark</h1>
            <a href="pickLocation.php" class="index-main-welcome-btn"><button>Start</button></a>
        </div>

    </main>
</body>
</html>