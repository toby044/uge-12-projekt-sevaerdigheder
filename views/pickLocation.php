<html>
<?php
include('../modules/head.php');
include('../modules/header.php');
include('../rest/test_api.php');
?>
<div class="picklocation-wrapper pad-wrap">
  <div class="picklocation-left">

    <h1>Choose a municipality to begin</h1>
    <p>To use this tool, you will need to pick a munucipality to begin the process of finding ancient monuments for your trip !</p>
    
    <form method="POST" action="pickAttraction.php">
      <select class="select-location" id="kommuneselect" name="selectkommune" onchange="this.form.submit()">
        <?php
        $page = getKommuner(null);
        echo '<option selected disabled>Select municipality</option>';
        foreach ($page as $data) {
          echo '<option 
                value="' . $data['kommune_kode'] . '"';

          if ($_POST) {
            if ($data['kommune_kode'] == $_POST['selectkommune']) {
              echo 'selected';
            }
          }

          echo '>';
          echo $data['kommune_navn'];
          echo '</option>';
        }
        ?>
      </select>
    </form>
  </div>
  <div id="slider">
    <figure>
      <img src="https://www.visitodsherred.dk/sites/visitodsherred.com/files/2019-11/oldtidsminder-udsigter-fortidsminder-jaettestuer-gravhoeje-dysser-odsherred-sjaelland-danmark-01.jpg" alt="Odsherred gravhøj">
      <img src="https://www.moesgaardmuseum.dk/media/6805/fortidsminder-5.jpg?anchor=center&mode=crop&width=960&heightratio=0.5619747899159663865546218487&format=jpg&quality=90&slimmage=true&rnd=132455095300000000" alt="">
      <img src="https://www.moesgaardmuseum.dk/media/6806/fortidsminder-4.jpg?anchor=center&mode=crop&width=960&heightratio=0.5619747899159663865546218487&format=jpg&quality=90&slimmage=true&rnd=132455095300000000" alt="">
      <img src="https://www.moesgaardmuseum.dk/media/6804/fortidsminder-3.jpg?anchor=center&mode=crop&width=960&heightratio=0.5619747899159663865546218487&format=jpg&quality=90&slimmage=true&rnd=132455095290000000" alt="">
    </figure>
  </div>
</div>

</html>