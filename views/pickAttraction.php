<html>
<?php
include('../modules/head.php');
include('../modules/header.php');
include('../rest/test_api.php');
if ($_POST) {
?>
  <div class="picklocation-wrapper pad-wrap">
    <div class="pickattraction-left">
      <h1>Choose an ancient monument</h1>
      <p>Now you can choose a monument from the dropdown. When you have chosen and selected one, you will be redirected to get more information including adress, city, etc. </p>

      <form method="POST" action="showInfo.php">
        <select class="select-attraction" id="fortidsmindeselect" name="selectfortidsminde" onchange="this.form.submit()">
          <?php
          $selectedKommune = $_POST['selectkommune'];
          $fortidsminder = getFortidsminder($selectedKommune);
          echo '<option selected disabled>Select monument</option>';
          foreach ($fortidsminder as $minde) {
            if ($selectedKommune == $minde['kommune_kode']) {
              echo '<option 
            value="' . $minde['id'] . '"';
            }
            echo '>';
            echo $minde['navn'];
            echo '</option>';
          }
          ?>
        </select>
        <!-- hidden fields to contain passable values -->
        <input type="hidden" name="selectkommune" value="<?= $selectedKommune; ?>">
      </form>
    </div>

    <div id="map" style="min-height: 100%; width: 80%"></div>
    <?php 
      //Get Kommune coordinates
      $kommune = getKommune($selectedKommune);
      $kommune_lat = $kommune['visueltcenter'][1];
      $kommune_lng = $kommune['visueltcenter'][0];

    ?>
    <script>
      var map = L.map('map').setView([<?= $kommune_lat?>, <?=$kommune_lng?>], 10);

      L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibW9ydGVucnVkIiwiYSI6ImNsMTVrOWsxZzB0c3AzbHJ0aDJ5ZHdxaWUifQ.d0ehjvZ-RQyxTn_VQTZtvw'

      }).addTo(map);

      L.control.scale({maxWidth: 200, metric: true}).addTo(map);
    </script>

  </div>

<?php } ?>
<div class="pickattraction-bg pad-wrap">
  <div class="pickattraction-info">
    <?php
    $selectedKommune = $_POST['selectkommune'];
    $fortidsminder = getFortidsminder($selectedKommune);
    foreach ($fortidsminder as $minde) {
      if ($selectedKommune == $minde['kommune_kode']) {
        echo '<div class="pickattraction-info-box">';
        echo '<h3>' . $minde['navn'] . '</h3>' . '<br>';
        echo ucfirst($minde['undertype']) . '<br>';
        echo 'Kommune: ' . $minde['kommune_navn'] . '<br>';
        echo 'Kommunekode: ' . $minde['kommune_kode'] . '<br>';
        echo 'Koordinater: ' . $minde['latitude'] . ',' . $minde['longitude'];
        echo '</div>';
      }
    ?>
      <script>
        var marker = L.marker([<?= $minde['latitude'] ?>, <?= $minde['longitude'] ?>]).addTo(map);
        marker.bindPopup(`
        <?php
        echo '<h3>' . $minde['navn'] . '</h3>';
        echo '<i>'.$minde['undertype'].'</i>';
        ?>
        `);
      </script>
    <?php
    }
    ?>
  </div>
</div>


</html>