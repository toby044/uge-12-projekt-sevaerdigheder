<html>
<?php
include('../modules/head.php');
include('../modules/header.php');
include('../rest/test_api.php');

// var_dump($_POST);

$id = $_POST['selectfortidsminde'];

$minde = getFortidsminde($id);
$minde = $minde[0];

$forplejning_arr = getForplejning();

// var_dump($forplejning_arr);

//All properties for your taking
echo $minde->id;
echo $minde->unique_id;
echo $minde->hovedtype;
echo $minde->undertype;
echo $minde->navn;
echo $minde->kommune_kode;
echo $minde->kommune_navn;
echo $minde->latitude;
echo $minde->longitude;
echo '<br><hr /><br>';



/** Start your fine print here */


?>

<div id="map" style="min-width:50%; min-height:40rem; padding: 1rem"></div>

<script>
    var map = L.map('map').setView([<?= $minde->latitude ?>, <?= $minde->longitude ?>], 15);
    L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1IjoibW9ydGVucnVkIiwiYSI6ImNsMTVrOWsxZzB0c3AzbHJ0aDJ5ZHdxaWUifQ.d0ehjvZ-RQyxTn_VQTZtvw'

    }).addTo(map);

    L.control.scale({
        maxWidth: 200,
        metric: true
    }).addTo(map);

    var marker = L.marker([<?= $minde->latitude ?>, <?= $minde->longitude ?>]).addTo(map);
    marker.bindPopup(`
        <?php
        echo '<h3>' . $minde->navn . '</h3>';
        echo '<i>' . $minde->undertype . '</i>';
        ?>
        `).openPopup();

    var foodIcon = new L.Icon({
        iconUrl: '../img/food_location_marker_icon.svg',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
</script>

<?php
foreach ($forplejning_arr as $object) {
?>

    <script>
        var marker = L.marker([<?= $object->latitude ?>, <?= $object->longitude ?>], {icon:foodIcon}).addTo(map);
        marker.bindPopup(`
        <?php
        echo '<h3>' . $object->navn . '</h3>';
        echo '<i>'
            . $object->adresse . ', '
            . $object->postnr . ' ' . $object->by
            . '</i>';
            echo '<p><small style="color: red">'
            . $object->branche_undertype
            . '</small></p>';
        ?>
        `);
    </script>

<?php
}
?>



</html>