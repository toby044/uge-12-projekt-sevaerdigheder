<?php
require_once 'db.php';

set_time_limit(600);

// Check if file exists
if (file_exists('datafiles/allekontrolresultater.xml')) {

    // load file with simplexml_load_file
    $xml = simplexml_load_file('datafiles/allekontrolresultater.xml');
}

//Execute cleanup procedure
cleanTable('forplejning');

// Execute recursive function
echo '<h1>Reading XML and inserting to database. Please wait...</h1>';

readNode($xml);

echo '<h2> A lot of records inserted</h2>';



/***
 * 
 * FUNCTIONS
 */

//Clean the table before insert
function cleanTable($table)
{
    echo '<h1>Cleaning table</h1>';
    // Connect to db
    $con = db::connect();
    echo '<h2>Connection established</h2>';
    // Clean tables to prepare for new import
    $sql = "TRUNCATE TABLE `$table`";
    $con->query($sql);
    $con->close();
    echo '<h1>Connection closed</h1>';
}

// read first node
function readNode($xml)
{
    //open database connection. Close at end of function.
    $con = db::connect();

    //refactoring
    $att = $xml->attributes();

    $postnr = array(6000, 7000, 7100, 7120, 7130, 7140, 7190);
    $branchekoder = array("47.11.00.A", "56.10.00.A", "56.10.00.B", "47.24.00.A", "47.24.00.B");
    $min_smileys = 2;

    if (
        in_array($att->postnr, $postnr) &&
        in_array($att->brancheKode, $branchekoder) &&
        $att->seneste_kontrol <= $min_smileys &&
        $att->Geo_Lat > 0 && $att->Geo_Lng > 0
    ) {

        $cvrnr = htmlspecialchars($att->cvrnr);
        $branche_kode = htmlspecialchars($att->brancheKode);
        $branche_type = htmlspecialchars($att->branche);
        $branche_undertype = htmlspecialchars($att->pixibranche);
        $navn = htmlspecialchars($att->navn1);
        $adresse = htmlspecialchars($att->adresse1);
        $postnr = htmlspecialchars($att->postnr);
        $by = htmlspecialchars($att->By);
        $latitude = htmlspecialchars($att->Geo_Lat);
        $longitude = htmlspecialchars($att->Geo_Lng);

        //Inserting to database
        // Connect to db

        $sql = "INSERT INTO sightseeing.forplejning (
            cvrnr, 
            branche_kode, 
            branche_type, 
            branche_undertype, 
            navn, 
            adresse, 
            postnr, 
            `by`, 
            latitude, 
            longitude
            ) " .
            "VALUES (
                '$cvrnr', 
                '$branche_kode', 
                '$branche_type', 
                '$branche_undertype', 
                '$navn',
                '$adresse',
                '$postnr',
                '$by',
                '$latitude',
                '$longitude'
                )";

        // var_dump($sql);
        $con->query($sql);
        // exit;
        
    }

    // Check for children
    foreach ($xml->children() as $node) {
        // If element has children, traverse
        readNode($node);
    }
    $con->close();
}

