DROP DATABASE IF EXISTS `sightseeing`;

CREATE DATABASE `sightseeing`;

CREATE TABLE `sightseeing`.`fortidsminde` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `unique_id` VARCHAR(200) NOT NULL,
    `hovedtype` VARCHAR(200) NOT NULL,
    `undertype` VARCHAR(200) NOT NULL,
    `navn` VARCHAR(200) NOT NULL,
    `kommune_kode` VARCHAR(4) NOT NULL,
    `kommune_navn` VARCHAR(200) NOT NULL,
    `latitude` FLOAT NOT NULL,
    `longitude` FLOAT NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

CREATE TABLE `sightseeing`.`forplejning` (
    `id` INT NOT NULL AUTO_INCREMENT,
    `cvrnr` VARCHAR(200) NOT NULL,
    `branche_kode` VARCHAR(200) NOT NULL,
    `branche_type` VARCHAR(200) NOT NULL,
    `branche_undertype` VARCHAR(200) NOT NULL,
    `navn` VARCHAR(200) NOT NULL,
    `adresse` VARCHAR(200) NOT NULL,
    `postnr` VARCHAR(4) NOT NULL,
    `by` VARCHAR(200) NOT NULL,
    `latitude` FLOAT NOT NULL,
    `longitude` FLOAT NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB;

SET
    GLOBAL max_allowed_packet = 524288000;