<?php
require_once 'db.php';

// URI to resource
$url = "https://api.dataforsyningen.dk/steder?hovedtype=Fortidsminde";

// Initiate the connection
$cn = curl_init($url);

// Use GET
curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

// Execute
$result = curl_exec($cn);

$result = json_decode($result);

//TODO: Connect to db
$con = db::connect();

// Clean tables to prepare for new import
$sql = "TRUNCATE TABLE fortidsminde";
$con->query($sql);

$counter = 0;
echo 'Adding to database. Please wait!';
//INSERT NEW DATA
foreach ($result as $item) {

    //$result = $result[0]; //Temp fix for one result. TODO: make foreach

    // Display the result
    // var_dump($result);

    $unique_id = htmlspecialchars((string)$item->id);
    $hovedtype = htmlspecialchars($item->hovedtype);
    $undertype = htmlspecialchars($item->undertype);
    if($undertype === "voldVoldsted") $undertype = "voldsted";   
    $navn = htmlspecialchars($item->primærtnavn);
    $kommune_kode = htmlspecialchars($item->kommuner[0]->kode);
    $kommune_navn = htmlspecialchars($item->kommuner[0]->navn);
    $latitude = htmlspecialchars($item->visueltcenter[1]);
    $longitude = htmlspecialchars($item->visueltcenter[0]);

    // var_dump($unique_id);
    // var_dump($hovedtype);
    // var_dump($undertype);
    // var_dump($navn);
    // var_dump($kommune_kode);
    // var_dump($kommune_navn);
    // var_dump($latitude);
    // var_dump($longitude);

    //TODO: Make insert statement.

    $sql = "INSERT INTO sightseeing.fortidsminde (
    unique_id, 
    hovedtype, 
    undertype, 
    navn, 
    kommune_kode, 
    kommune_navn, 
    latitude, 
    longitude
    ) " .
        "VALUES (
        '$unique_id', 
        '$hovedtype', 
        '$undertype', 
        '$navn', 
        '$kommune_kode', 
        '$kommune_navn',
        '$latitude',
        '$longitude'
        )";

    // var_dump($sql);
    $con->query($sql);
    $counter++;
}

$con->close();
echo '<br>' . $counter . ' records was added to database';