<?php
// Check if file exists
if (file_exists('datafiles/allekontrolresultater.xml')) {

   // load file with simplexml_load_file
   $xml = simplexml_load_file('datafiles/allekontrolresultater.xml');
}

// Execute recursive function
readNode($xml);

// read first node
function readNode($xml)
{
   $counter = 0;
   $att = $xml->attributes();
   $postnr = array(6000);
   $branchekoder = array("56.10.00.B", "47.24.00.A", "47.24.00.B");
   $min_smileys = 2;

   // foreach ($att as $key => $item) {
   //    echo '<p>'.$key . " => ";
   //    echo $item . '</p>';
   // }


   if (
      in_array($xml->attributes()->postnr, $postnr) &&
      in_array($xml->attributes()->brancheKode, $branchekoder) &&
      $xml->attributes()->seneste_kontrol <= $min_smileys
   ) {
      // $cvrdata = getCvr($xml->attributes()->cvrnr);
      // if (!is_null($cvrdata->enddate)) return;

      echo '<p>';
      echo  $xml->attributes()->navn1 . ' ';
      echo  $xml->attributes()->postnr. ' ';
      echo  $xml->attributes()->By. ' ';
      echo  $xml->attributes()->seneste_kontrol_dato. ' ';
      echo  $xml->attributes()->seneste_kontrol. ' ';
      echo  $xml->attributes()->Geo_Lng. ' ';
      echo  $xml->attributes()->Geo_Lat. ' ';

      $masts = getMast($xml->attributes()->Geo_Lng, $xml->attributes()->Geo_Lat);

      echo '<br />';
      echo "Nærmeste mast lokation: " . $masts["vejnavn"] . " " . $masts["husnr"]. ' ';
      
      echo "Afstand: ";
      
      $afstand = (distance(
         (float)$xml->attributes()->Geo_Lat,
         (float)$xml->attributes()->Geo_Lng,
         (float)$masts["lat"],
         (float)$masts["long"]
      ));
      echo $afstand * 1000 . "meter";
      
      echo '</p>';
      // exit;
      
   }

   // Check for children
   foreach ($xml->children() as $node) {
      // If element has children, traverse
      readNode($node);
      // exit;
   }
}


function getMast($lng, $lat)
{
   // URI to resource
   $url = "https://mastedatabasen.dk/Master/antenner/$lat,$lng.json";
   // Initiate the connection
   $cn = curl_init($url);

   if ($cn == false) {
      die("no conn");
   }

   // Use GET
   // Set headers
   $headers = array("Accept: application/json");
   curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($cn, CURLOPT_HTTPHEADER, $headers);

   // Execute
   $page = curl_exec($cn);

   $data = json_decode($page);

   return array(
      "vejnavn" => $data->vejnavn->navn,
      "husnr" => $data->husnr,
      "lat" => $data->wgs84koordinat->bredde,
      "long" => $data->wgs84koordinat->laengde
   );
}

function getCvr($cvr)
{
   // URI to resource
   $url = "https://cvrapi.dk/api?search=$cvr&country=dk";

   // Initiate the connection
   $cn = curl_init($url);

   if ($cn == false) {
      return;
   }

   // Use GET
   curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
   curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

   // Execute
   $page = curl_exec($cn);
   return json_decode($page);
}

function distance(
   $latitudeFrom,
   $longitudeFrom,
   $latitudeTo,
   $longitudeTo
) {
   $long1 = deg2rad($longitudeFrom);
   $long2 = deg2rad($longitudeTo);
   $lat1 = deg2rad($latitudeFrom);
   $lat2 = deg2rad($latitudeTo);

   //Haversine Formula
   $dlong = $long2 - $long1;
   $dlati = $lat2 - $lat1;

   $val = pow(sin($dlati / 2), 2) + cos($lat1) * cos($lat2) * pow(sin($dlong / 2), 2);

   $res = 2 * asin(sqrt($val));

   $radius = 3958.756;

   return ($res * $radius * 1.609344);
}
