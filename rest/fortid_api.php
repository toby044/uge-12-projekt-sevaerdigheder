<?php
require_once 'class_api.php';
require_once 'db.php';

//FEATURE: 1: Fetch restauranter i $postnr, som ikke har været besøgt i X måneder. (75 måneder)
//TODO: 2: Fetch restauranter i $postnr, med minimum smiley, og max meter til mobilmast.
//[v]:Tilføj adresse i database og koble op til mastedatabasen.dk
//TODO: 3: Fetch restauranter i $postnr, som ligger indenfor en radius af X meter, en adresse. (DAWA api)
//:Koble op til Dawa api


class fortid_api extends API
{
    protected $con;

    public function __construct($request, $origin)
    {
        parent::__construct($request);
        $this->con = db::connect();

        /* TODO: IMPLEMENT AUTHENTICATION
            $APIKey = new APIKey();
            $User = new User();

            // Throw error if no API key detected, or if user not authenticated
            if (!array_key_exists('apiKey', $this->request)) {
                throw new Exception('No API Key provided');
            } else if (!$APIKey->verifyKey($this->request['apiKey'], $origin)) {
                throw new Exception('Invalid API Key');
            } else if (array_key_exists('token', $this->request) &&
                 !$User->get('token', $this->request['token'])) {

                throw new Exception('Invalid User Token');
            } */
    }

    // Endpoint for fortidsminder. Returns all fortidsminder
    protected function fortidsminder($args)
    {

        if ($this->method == 'GET') {

            if (!$args) {

                $sql = 'SELECT * from fortidsminde';
            } else {
                $sql = "SELECT * from fortidsminde WHERE kommune_kode = '$args[0]'";
            }

            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
    protected function fortidsminde($args)
    {

        if ($this->method == 'GET') {

            
            $sql = "SELECT * from fortidsminde WHERE id = '$args[0]'";

            $results = $this->con->query($sql);


            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
    protected function kommuner($args)
    {

        if ($this->method == 'GET') {

            if (!$args) {

                $sql = 'SELECT DISTINCT `kommune_kode`,`kommune_navn` 
                FROM `fortidsminde` 
                ORDER BY `fortidsminde`.`kommune_navn` ASC';
            } else {
                $sql = "SELECT DISTINCT `kommune_kode`,`kommune_navn` 
                FROM `fortidsminde` 
                WHERE kommune_kode = '$args[0]' 
                ORDER BY `fortidsminde`.`kommune_navn` ASC";
            }

            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
    protected function forplejning($args)
    {
        // var_dump($args);

        if ($this->method == 'GET') {

            if (!$args) {
                $sql = 'SELECT DISTINCT `branche_kode`,`branche_type`, `branche_undertype` 
                FROM `forplejning` 
                ORDER BY `forplejning`.`branche_undertype` ASC';
            } 
            elseif ($args[0] == "alle") {
                $sql = "SELECT *
                FROM `forplejning` 
                ORDER BY `forplejning`.`navn` ASC";
            } 
            else {
                $sql = "SELECT *
                FROM `forplejning` 
                WHERE branche_kode = '$args[0]'
                ORDER BY `forplejning`.`navn` ASC";
            }

            $results = $this->con->query($sql);

            return mysqli_fetch_all($results, MYSQLI_ASSOC);
        } else {
            return "Only accepts GET requests";
        }
    }
}
