<?php
    class db {

        static function connect() {
            $con = mysqli_connect('localhost', 'root', '', 'sightseeing', '3306');
            if (!$con) {
                die('Could not connect to MySQL: ' . mysqli_connect_error());
            }

            return $con;
        }
    }
?>