<?php
    require "fortid_api.php";

    if (!array_key_exists('HTTP_ORIGIN', $_SERVER)) {
        // Requests from the same server don't have a HTTP_ORIGIN header
        $_SERVER['HTTP_ORIGIN'] = $_SERVER['SERVER_NAME'];
    }

    // Serve the request
    try {
        $API = new fortid_api($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
        echo $API->processAPI();

    } catch (Exception $e) {
        echo json_encode(Array('error' => $e->getMessage()));
    }
    
    // // Serve the request
    // try {
    //     $API = new events_api($_REQUEST['request'], $_SERVER['HTTP_ORIGIN']);
    //     echo $API->processAPI();

    // } catch (Exception $e) {
    //     echo json_encode(Array('error' => $e->getMessage()));
    // }
?>
