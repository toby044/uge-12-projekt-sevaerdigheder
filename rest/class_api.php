<?php
    abstract class API {
        // The HTTP method this request was made in, either GET, POST, PUT or DELETE
        protected $method = '';

        //The Model requested in the URI. eg: /files
        protected $endpoint = '';

        // An optional additional descriptor about the endpoint, used for things that can
        // not be handled by the basic methods. eg: /files/process
        protected $verb = '';

        //Any additional URI components after the endpoint and verb have been removed, in our
        // case, an integer ID for the resource. eg: /<endpoint>/<verb>/<arg0>/<arg1> or /<endpoint>/<arg0>
        protected $args = Array();

        // Stores the input of the PUT request
        protected $file = Null;

        public function __construct($request) {
            // Allow calls from outside our domain, eg. any origin
            header("Access-Control-Allow-Orgin: *");

            // Allow all method calls
            header("Access-Control-Allow-Methods: *");

            // Return in JSON format
            header("Content-Type: application/json");

            // Get the arguments as an array
            $this->args = explode('/', rtrim($request, '/'));
            $this->endpoint = array_shift($this->args);
            if (array_key_exists(0, $this->args) && !is_numeric($this->args[0])) {
                $this->verb = array_shift($this->args);
            }

            // Figure out which reguest methid is used.
            $this->method = $_SERVER['REQUEST_METHOD'];

            // If the methods are described as cross domain http methods, set the method
            if ($this->method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
                if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') {
                    $this->method = 'DELETE';
                } else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') {
                    $this->method = 'PUT';
                } else {
                    throw new Exception("Unexpected Header");
                }
            }

            switch($this->method) {
                case 'DELETE':
                    break;
                case 'POST':
                    $this->request = $this->_cleanInputs($_POST);
                    break;
                case 'GET':
                    $this->request = $this->_cleanInputs($_GET);
                    break;
                case 'PUT':
                    // Put code here if you want to be able to upload files
                    break;
                default:
                    $this->_response('Invalid Method', 405);
                    break;
            }
        }

        public function processAPI() {
            // Make sure the method exists.
            if (method_exists($this, $this->endpoint)) {
                return $this->_response($this->{$this->endpoint}($this->args));
            }

            // Else return the error
            return $this->_response("No Endpoint: $this->endpoint", 404);
        }

        private function _response($data, $status = 200) {
            // Pr. default status code 200 OK
            header("HTTP/1.1 " . $status . " " . $this->_requestStatus($status));
            return json_encode($data);
        }

        private function _cleanInputs($data) {
            $input = Array();
            if (is_array($data)) {
                foreach ($data as $key => $val) {
                    // Recursive cleaning of parameters
                    $input[$key] = $this->_cleanInputs($val);
                }
            } else {
                // Remove the tags and remove trailing spaces
                $input = trim(strip_tags($data));
            }
            return $input;
        }

        private function _requestStatus($code) {
            $msg = '';
            switch ($code): 
                 case 200:
                     $msg = 'OK';
                     break;
                 case 404:
                     $msg = "Not Found";
                     break;
                 case 405:
                     $msg = "Method not allowed";
                     break;
                 case 500:
                     $msg = "Internal Server Error";
                     break;
                 default:
                     $msg = "Internal Server Error";
                     break;
            endswitch;
            return $msg; 
        }
    }