<?php

// Connections
function getKommuner($kommunekode)
{
    if (!$kommunekode) {
        $url = "http://localhost/uge-12-projekt-sevaerdigheder/rest/REST/api/v1/kommuner/";
    } else {
        $url = "http://localhost/uge-12-projekt-sevaerdigheder/rest/REST/api/v1/kommuner/" . $kommunekode;
    }

    $cn = curl_init($url);

    curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

    $page = curl_exec($cn);

    $page = json_decode($page, true);
    curl_close($cn);
    return $page;
}

function getKommune($kommunekode)
{

        $url = "https://api.dataforsyningen.dk/kommuner/$kommunekode";
    

    $cn = curl_init($url);

    curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

    $page = curl_exec($cn);

    $page = json_decode($page, true);
    // close cURL resource, and free up system resources
    curl_close($cn);
    return $page;
}

function getFortidsminder($kommunekode)
{
    if (!$kommunekode) {
        $url = "http://localhost/uge-12-projekt-sevaerdigheder/rest/REST/api/v1/fortidsminder/";
    } else {
        $url = "http://localhost/uge-12-projekt-sevaerdigheder/rest/REST/api/v1/fortidsminder/" . $kommunekode;
    }

    $cn = curl_init($url);

    curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

    $page = curl_exec($cn);

    $page = json_decode($page, true);
    curl_close($cn);
    return $page;
}

function getFortidsminde($id)
{

    $url = "http://localhost/uge-12-projekt-sevaerdigheder/rest/REST/api/v1/fortidsminde/" . $id;


    $cn = curl_init($url);

    curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

    $page = curl_exec($cn);

    $page = json_decode($page);
    curl_close($cn);
    return $page;
}
function getForplejning()
{

    // $url = "http://localhost/uge-12-projekt-sevaerdigheder/rest/REST/api/v1/forplejning/branche/56.10.00.B";
    $url = "http://localhost/uge-12-projekt-sevaerdigheder/rest/REST/api/v1/forplejning/branche/alle";


    $cn = curl_init($url);

    curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');

    $page = curl_exec($cn);

    $page = json_decode($page);
    curl_close($cn);
    return $page;
}

function getAdresse($adresseString)
{
    $vasketAdresse = vaskAdresse($adresseString);

    //take the href for the precise address
    $adresse_url = $vasketAdresse->resultater[0]->adresse->href;
    $adresse_id = $vasketAdresse->resultater[0]->adresse->id;

    // var_dump($adresse_url);
    // var_dump($adresse_id);
    
    //URL won't accept regular spaces, so using rawurlencode replaces whitespace with %20. 
    $url = "https://api.dataforsyningen.dk/adresser/$adresse_id"; 

    // Initiate the connection
    $cn = curl_init($url);
    if ($cn == false) {
        return;
     }

    curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
    
    $page = curl_exec($cn);

    $page = json_decode($page);
    curl_close($cn);
    return $page;
}

function vaskAdresse($adresseString)
{
    //URL won't accept regular spaces, so using rawurlencode replaces whitespace with %20. 
    $url = "https://api.dataforsyningen.dk/datavask/adresser?betegnelse=" . rawurlencode($adresseString); 

    // Initiate the connection
    $cn = curl_init($url);
    if ($cn == false) {
        return;
     }

    curl_setopt($cn, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($cn, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 GTB5');
    
    $page = curl_exec($cn);

    $page = json_decode($page);
    curl_close($cn);
    return $page;
}





// Display the page
    // var_dump($page);